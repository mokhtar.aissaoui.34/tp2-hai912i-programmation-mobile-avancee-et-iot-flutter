import 'package:exo2/bloc/quiz.cubit.dart';
import 'package:exo2/models/quiz.model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class QuizCubitPage extends StatelessWidget {
  const QuizCubitPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Question/réponse"),
      ),
      backgroundColor: Colors.blueGrey,
      body: BlocBuilder<QuizCubit, quizModel>(
          builder: (context, state) => Stack(children: <Widget>[
                Container(
                    height: 600,
                    width: 400,
                    padding: EdgeInsets.fromLTRB(50, 30, 50, 30),
                    margin: EdgeInsets.fromLTRB(40, 0, 40, 450),
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                      image: NetworkImage(
                          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSsuLP2gvaNxSQb1_ivpbKi2IOiNP4FgBLAA&usqp=CAU"),
                    ))),
                Center(
                    child: Text(
                        context
                            .read<QuizCubit>()
                            .state
                            .getQuestion()[
                                context.read<QuizCubit>().state.getindex()]
                            .questiontext,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          backgroundColor:
                              context.read<QuizCubit>().state.getbacktext(),
                        ))),
                Positioned(
                  top: 450,
                  left: 55,
                  child: ElevatedButton(
                    onPressed:
                        context.read<QuizCubit>().state.getisButtonDisabled()
                            ? null
                            : () => context.read<QuizCubit>().btnVrai(),
                    child: Text(
                      'Vrai',
                    ),
                  ),
                ),
                Positioned(
                  top: 450,
                  left: 155,
                  child: ElevatedButton(
                    onPressed:
                        context.read<QuizCubit>().state.getisButtonDisabled()
                            ? null
                            : () => context.read<QuizCubit>().btnFaux(),
                    child: Text(
                      'Faux',
                    ),
                  ),
                ),
                Positioned(
                  top: 450,
                  left: 255,
                  child: ElevatedButton(
                    onPressed: !context
                            .read<QuizCubit>()
                            .state
                            .getisButtonDisabled()
                        ? null
                        : () => {
                              print(context.read<QuizCubit>().state.getindex()),
                              context.read<QuizCubit>().btnSuivant()
                            },
                    child: Text(
                      '-->',
                    ),
                  ),
                ),
                Positioned(
                    top: 10,
                    left: 10,
                    child: Text(
                        "Score :  ${context.read<QuizCubit>().state.getscore()}/ ${context.read<QuizCubit>().state.getQuestionCount()}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        )))
              ])),
    );
  }
}
