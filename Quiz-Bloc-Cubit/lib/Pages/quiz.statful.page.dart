import 'package:flutter/material.dart';
class MyQuizPage extends StatefulWidget {
  const MyQuizPage({Key? key, required this.title}) : super(key: key);


  final String title;


  @override
  State<MyQuizPage> createState() => _MyQuizPageState();
}
class Question {
  String questiontext;
  bool isCorrct;
  Question({required this.questiontext,required this.isCorrct});

}
class _MyQuizPageState extends State<MyQuizPage> {
  int _score = 0;
  int _index = 0;
  bool _isButtonDisabled = false;
  Color _backtext = Colors.transparent;
  final List<Question> _questions= [
    Question(questiontext: "L'est de l'Espagne est bordé par l'océan Atlantique ?", isCorrct: false),
    Question(questiontext: "Le taureau est le mâle de la vache ?", isCorrct: true),
    Question(questiontext: "Les hommes ont plus de poils que les femmes", isCorrct: false),
    Question(questiontext: "Le micro-onde peut perturber les connexions Wi-Fi", isCorrct: true)
  ];

  void _btnVrai() {
    if (_questions[_index].isCorrct) {
      setState((){
        _backtext = Colors.green;
        _score++;
        _isButtonDisabled = true;
      });

    } else {
      setState((){
        _backtext = Colors.red;
        _isButtonDisabled = true;
      });
    }
    if(_index == _questions.length-1){
      setState((){
        _questions[_index].questiontext ="votre score est : $_score /"+_questions.length.toString();
      });
    }
  }
  void _btnFaux() {
    setState((){
      if (!_questions[_index].isCorrct) {
        _backtext = Colors.green;
        _score++;
        _isButtonDisabled = true;
      } else {
        _backtext = Colors.red;
        _isButtonDisabled = true;
      }
      if(_index == _questions.length-1){
        _questions[_index].questiontext ="votre score est : $_score /"+_questions.length.toString();
      }
    });
  }

  void _btnSuivant(){
    setState((){
      if(_index<_questions.length-1){
        _index++;
        _backtext = Colors.transparent;
        _isButtonDisabled=false;
      }else{
        _index=0;
        _score=0;
        _backtext = Colors.transparent;
        _isButtonDisabled=false;

      }
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      backgroundColor: Colors.blueGrey,
      body: Stack(

          children: <Widget> [
            Container(
                height: 600,
                width: 400,
                padding: EdgeInsets.fromLTRB(50,30,50,30),
                margin: EdgeInsets.fromLTRB(40,0,40,450),
                decoration:  const BoxDecoration(
                    image: DecorationImage( image :NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSsuLP2gvaNxSQb1_ivpbKi2IOiNP4FgBLAA&usqp=CAU"),)
                )),
            Center(
                child: Text(
                    _questions[_index].questiontext,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      backgroundColor: _backtext,
                    ))),
            Positioned(top: 450,left: 55, child: FlatButton(
              onPressed:  _isButtonDisabled ? null : _btnVrai,
              child: Text('Vrai',),
              color: Colors.green,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
            ),),
            Positioned(top: 450,left: 155, child: FlatButton(
              onPressed: _isButtonDisabled ? null : _btnFaux ,
              child: Text('Faux',),
              color: Colors.red,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
            ),),
            Positioned(top: 450,left: 255, child: FlatButton(
              onPressed: !_isButtonDisabled ? null : _btnSuivant ,
              child: Text('-->',),
              color: Colors.green,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
            ),),
            Positioned(top : 10,
                left: 10,
                child: Text("Score : $_score /"+_questions.length.toString(), style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,)))


          ]
      ),

    );
  }
}