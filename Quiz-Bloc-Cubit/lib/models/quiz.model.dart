import 'package:exo2/Pages/quiz.statful.page.dart';
import 'package:flutter/material.dart';

class quizModel {
  int _score = 0;
  int _index = 0;
  bool _isButtonDisabled = false;
  Color _backtext = Colors.transparent;
  final List<Question> _questions = [
    Question(
        questiontext: "L'est de l'Espagne est bordé par l'océan Atlantique ?",
        isCorrct: false),
    Question(
        questiontext: "Le taureau est le mâle de la vache ?", isCorrct: true),
    Question(
        questiontext: "Les hommes ont plus de poils que les femmes",
        isCorrct: false),
    Question(
        questiontext: "Le micro-onde peut perturber les connexions Wi-Fi",
        isCorrct: true)
  ];
  quizModel() {}
  void setindex(int index) {
    _index = index;
  }

  void addscore(int index) {
    _score += index;
  }

  void setDisable(bool b) {
    _isButtonDisabled = b;
  }

  void setBackText(Color backText) {
    _backtext = backText;
  }

  List<Question> getQuestion() {
    return _questions;
  }

  int getindex() {
    return _index;
  }

  int getscore() {
    return _score;
  }

  int getQuestionCount() {
    return _questions.length;
  }

  bool getisButtonDisabled() {
    return _isButtonDisabled;
  }

  void viewScore() {
    _questions[_index].questiontext =
        "votre score est : $_score /" + _questions.length.toString();
  }

  Color getbacktext() {
    return _backtext;
  }
}
