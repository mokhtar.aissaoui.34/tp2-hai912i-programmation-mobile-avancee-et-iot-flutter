import 'package:exo2/Pages/quiz.provider.page.dart';
import 'package:exo2/Provider/quizstate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Pages/quiz.statful.page.dart';

void main() {
  runApp(ChangeNotifierProvider(create: (context) => QuizState(),
      child :const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.grey,
          ),
          home: const QuizProviderPage(),
        );
  }
}
