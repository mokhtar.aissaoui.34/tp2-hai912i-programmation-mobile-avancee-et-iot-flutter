import 'package:exo2/Provider/quizstate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class QuizProviderPage extends StatelessWidget {
  const QuizProviderPage({Key? key}) : super(key: key);
  void _btnVrai(BuildContext context) {
    Provider.of<QuizState>(context, listen: false).btnVrai();
  }

  void _btnSuivant(BuildContext context) {
    Provider.of<QuizState>(context, listen: false).btnSuivant();
  }

  void _btnFaux(BuildContext context) {
    Provider.of<QuizState>(context, listen: false).btnFaux();
  }

  @override
  Widget build(BuildContext context) {
    var provid = Provider.of<QuizState>(context, listen: false);

    return Consumer<QuizState>(builder: (context, cart, child) {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Qyestion/réponse"),
        ),
        backgroundColor: Colors.blueGrey,
        body: Stack(children: <Widget>[
          Container(
              height: 600,
              width: 400,
              padding: EdgeInsets.fromLTRB(50, 30, 50, 30),
              margin: EdgeInsets.fromLTRB(40, 0, 40, 450),
              decoration: const BoxDecoration(
                  image: DecorationImage(
                image: NetworkImage(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSsuLP2gvaNxSQb1_ivpbKi2IOiNP4FgBLAA&usqp=CAU"),
              ))),
          Center(
              child: Text(provid.getQuestion(),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    backgroundColor: provid.getbacktext(),
                  ))),
          Positioned(
            top: 450,
            left: 55,
            child: ElevatedButton(
              onPressed:
                  provid.getisButtonDisabled() ? null : () => provid.btnVrai(),
              child: Text(
                'Vrai',
              ),
            ),
          ),
          Positioned(
            top: 450,
            left: 155,
            child: ElevatedButton(
              onPressed:
                  provid.getisButtonDisabled() ? null : () => provid.btnFaux(),
              child: Text(
                'Faux',
              ),
            ),
          ),
          Positioned(
            top: 450,
            left: 255,
            child: ElevatedButton(
              onPressed: !provid.getisButtonDisabled()
                  ? null
                  : () => provid.btnSuivant(),
              child: Text(
                '-->',
              ),
            ),
          ),
          Positioned(
              top: 10,
              left: 10,
              child: Text(
                  "Score :  ${provid.getscore()}/ ${provid.getQuestionCount()}",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  )))
        ]),
      );
    });
  }
}
