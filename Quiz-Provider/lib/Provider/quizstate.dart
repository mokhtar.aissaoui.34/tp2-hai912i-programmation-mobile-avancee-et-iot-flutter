import 'package:exo2/Pages/quiz.statful.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class QuizState extends ChangeNotifier {
  int _score = 0;
  int _index = 0;
  bool _isButtonDisabled = false;
  Color _backtext = Colors.transparent;
  final List<Question> _questions = [
    Question(
        questiontext: "L'est de l'Espagne est bordé par l'océan Atlantique ?",
        isCorrct: false),
    Question(
        questiontext: "Le taureau est le mâle de la vache ?", isCorrct: true),
    Question(
        questiontext: "Les hommes ont plus de poils que les femmes",
        isCorrct: false),
    Question(
        questiontext: "Le micro-onde peut perturber les connexions Wi-Fi",
        isCorrct: true)
  ];

  int get score => _score;

  set score(int value) {
    _score = value;
  }

  void btnVrai() {
    if (_questions[_index].isCorrct) {
      _backtext = Colors.green;
      _score++;
      _isButtonDisabled = true;
    } else {
      _backtext = Colors.red;
      _isButtonDisabled = true;
    }
    if (_index == _questions.length - 1) {
      _questions[_index].questiontext =
          "votre score est : $_score /" + _questions.length.toString();
    }
    notifyListeners();
  }

  void btnFaux() {
    if (!_questions[_index].isCorrct) {
      _backtext = Colors.green;
      _score++;
      _isButtonDisabled = true;
    } else {
      _backtext = Colors.red;
      _isButtonDisabled = true;
    }
    if (_index == _questions.length - 1) {
      _questions[_index].questiontext =
          "votre score est : $_score /" + _questions.length.toString();
    }
    notifyListeners();
  }

  void btnSuivant() {
    if (_index < _questions.length - 1) {
      _index++;
      _backtext = Colors.transparent;
      _isButtonDisabled = false;
    } else {
      _index = 0;
      _score = 0;
      _backtext = Colors.transparent;
      _isButtonDisabled = false;
    }
    notifyListeners();
  }

  set index(int value) {
    _index = value;
  }

  String getQuestion() {
    return _questions[_index].questiontext;
  }

  int getindex() {
    return _index;
  }

  int getscore() {
    return _score;
  }

  int getQuestionCount() {
    return _questions.length;
  }

  bool getisButtonDisabled() {
    return _isButtonDisabled;
  }

  set isButtonDisabled(bool value) {
    _isButtonDisabled = value;
  }

  Color getbacktext() {
    return _backtext;
  }

  set backtext(Color value) {
    _backtext = value;
  }
}
