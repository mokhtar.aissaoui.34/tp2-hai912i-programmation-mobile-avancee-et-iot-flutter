// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/Models/finalModel.dart';
import 'package:meteo/bloc/meteo.cubit.dart';
import 'package:meteo/network/Network.meteo.dart';

import 'pages/meteo.page.dart';

void main() {
  try {
    Network()
        .getWeatherForecast(cityName: 'montpellier')
        .then((value) => runApp(MyApp(
              resu: value,
            )));
  } catch (e) {}
}

class MyApp extends StatelessWidget {
  finalModel resu;
  MyApp({
    Key? key,
    required this.resu,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [BlocProvider(create: (context) => MeteoCubit(resu))],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: MeteoPage(),
      ),
    );
  }
}
