import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'clouds.dart';
import 'main.dart';
import 'sys.dart';
import 'weather.dart';
import 'wind.dart';

class MyList {
  final int dt;
  final Main main;
  final List<Weather> weather;
  final Clouds clouds;
  final Wind wind;
  final int visibility;
  final int pop;
  final Sys sys;
  final String dt_txt;
  MyList({
    required this.dt,
    required this.main,
    required this.weather,
    required this.clouds,
    required this.wind,
    required this.visibility,
    required this.pop,
    required this.sys,
    required this.dt_txt,
  });

  Map<String, dynamic> toMap() {
    return {
      'dt': dt,
      'main': main.toMap(),
      'weather': weather?.map((x) => x.toMap())?.toList(),
      'clouds': clouds.toMap(),
      'wind': wind.toMap(),
      'visibility': visibility,
      'pop': pop,
      'sys': sys.toMap(),
      'dt_txt': dt_txt,
    };
  }

  factory MyList.fromMap(Map<String, dynamic> map) {
    return MyList(
      dt: map['dt'],
      main: Main.fromMap(map['main']),
      weather:
          List<Weather>.from(map['weather']?.map((x) => Weather.fromMap(x))),
      clouds: Clouds.fromMap(map['clouds']),
      wind: Wind.fromMap(map['wind']),
      visibility: map['visibility'],
      pop: map['pop']?.toInt(),
      sys: Sys.fromMap(map['sys']),
      dt_txt: map['dt_txt'],
    );
  }

  String toJson() => json.encode(toMap());

  factory MyList.fromJson(String source) => MyList.fromMap(json.decode(source));

  @override
  String toString() {
    return 'List(dt: $dt, main: $main, weather: $weather, clouds: $clouds, wind: $wind, visibility: $visibility, pop: $pop, sys: $sys, dt_txt: $dt_txt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MyList &&
        other.dt == dt &&
        other.main == main &&
        listEquals(other.weather, weather) &&
        other.clouds == clouds &&
        other.wind == wind &&
        other.visibility == visibility &&
        other.pop == pop &&
        other.sys == sys &&
        other.dt_txt == dt_txt;
  }

  @override
  int get hashCode {
    return dt.hashCode ^
        main.hashCode ^
        weather.hashCode ^
        clouds.hashCode ^
        wind.hashCode ^
        visibility.hashCode ^
        pop.hashCode ^
        sys.hashCode ^
        dt_txt.hashCode;
  }
}
