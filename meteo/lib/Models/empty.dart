import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'city.dart';
import 'list.dart';

class Empty {
  final String cod;
  final int message;
  final int cnt;
  final List<MyList> list;
  final City city;
  Empty({
    required this.cod,
    required this.message,
    required this.cnt,
    required this.list,
    required this.city,
  });

  Empty copyWith({
    String? cod,
    int? message,
    int? cnt,
    List<MyList>? list,
    City? city,
  }) {
    return Empty(
      cod: cod ?? this.cod,
      message: message ?? this.message,
      cnt: cnt ?? this.cnt,
      list: list ?? this.list,
      city: city ?? this.city,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'cod': cod,
      'message': message,
      'cnt': cnt,
      'list': list,
      'city': city.toMap(),
    };
  }

  factory Empty.fromMap(Map<String, dynamic> map) {
    List<MyList> f = [];
    for (int i = 0; i < map['list']?.length; i++) {
      f.add(MyList.fromMap(map['list']?[i]));
    }
    return Empty(
      cod: map['cod'],
      message: map['message']?.toInt(),
      cnt: map['cnt']?.toInt(),
      list: f,
      city: City.fromMap(map['city']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Empty.fromJson(String source) => Empty.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Empty(cod: $cod, message: $message, cnt: $cnt, list: $list, city: $city)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Empty &&
        other.cod == cod &&
        other.message == message &&
        other.cnt == cnt &&
        listEquals(other.list, list) &&
        other.city == city;
  }

  @override
  int get hashCode {
    return cod.hashCode ^
        message.hashCode ^
        cnt.hashCode ^
        list.hashCode ^
        city.hashCode;
  }
}
