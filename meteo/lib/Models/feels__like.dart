import 'dart:convert';

class Feels_like {
  final double day;
  final double night;
  final double eve;
  final double morn;
  Feels_like({
    required this.day,
    required this.night,
    required this.eve,
    required this.morn,
  });

  Feels_like copyWith({
    double? day,
    double? night,
    double? eve,
    double? morn,
  }) {
    return Feels_like(
      day: day ?? this.day,
      night: night ?? this.night,
      eve: eve ?? this.eve,
      morn: morn ?? this.morn,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'day': day,
      'night': night,
      'eve': eve,
      'morn': morn,
    };
  }

  factory Feels_like.fromMap(Map<String, dynamic> map) {
    return Feels_like(
      day: map['day']?.toDouble(),
      night: map['night']?.toDouble(),
      eve: map['eve']?.toDouble(),
      morn: map['morn']?.toDouble(),
    );
  }

  String toJson() => json.encode(toMap());

  factory Feels_like.fromJson(String source) => Feels_like.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Feels_like(day: $day, night: $night, eve: $eve, morn: $morn)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Feels_like &&
      other.day == day &&
      other.night == night &&
      other.eve == eve &&
      other.morn == morn;
  }

  @override
  int get hashCode {
    return day.hashCode ^
      night.hashCode ^
      eve.hashCode ^
      morn.hashCode;
  }
}