import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'feels__like.dart';
import 'temp.dart';
import 'weather.dart';

class MyList2 {
  final int dt;
  final int sunrise;
  final int sunset;
  final Temp temp;
  final Feels_like feels_like;
  final int pressure;
  final int humidity;
  final List<Weather> weather;
  final double speed;
  final int deg;
  final double gust;
  final int clouds;
  final int pop;
  MyList2({
    required this.dt,
    required this.sunrise,
    required this.sunset,
    required this.temp,
    required this.feels_like,
    required this.pressure,
    required this.humidity,
    required this.weather,
    required this.speed,
    required this.deg,
    required this.gust,
    required this.clouds,
    required this.pop,
  });

  MyList2 copyWith({
    int? dt,
    int? sunrise,
    int? sunset,
    Temp? temp,
    Feels_like? feels_like,
    int? pressure,
    int? humidity,
    List<Weather>? weather,
    double? speed,
    int? deg,
    double? gust,
    int? clouds,
    int? pop,
  }) {
    return MyList2(
      dt: dt ?? this.dt,
      sunrise: sunrise ?? this.sunrise,
      sunset: sunset ?? this.sunset,
      temp: temp ?? this.temp,
      feels_like: feels_like ?? this.feels_like,
      pressure: pressure ?? this.pressure,
      humidity: humidity ?? this.humidity,
      weather: weather ?? this.weather,
      speed: speed ?? this.speed,
      deg: deg ?? this.deg,
      gust: gust ?? this.gust,
      clouds: clouds ?? this.clouds,
      pop: pop ?? this.pop,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'dt': dt,
      'sunrise': sunrise,
      'sunset': sunset,
      'temp': temp.toMap(),
      'feels_like': feels_like.toMap(),
      'pressure': pressure,
      'humidity': humidity,
      'weather': weather?.map((x) => x.toMap())?.toList(),
      'speed': speed,
      'deg': deg,
      'gust': gust,
      'clouds': clouds,
      'pop': pop,
    };
  }

  factory MyList2.fromMap(Map<String, dynamic> map) {
    return MyList2(
      dt: map['dt']?.toInt(),
      sunrise: map['sunrise']?.toInt(),
      sunset: map['sunset']?.toInt(),
      temp: Temp.fromMap(map['temp']),
      feels_like: Feels_like.fromMap(map['feels_like']),
      pressure: map['pressure']?.toInt(),
      humidity: map['humidity']?.toInt(),
      weather:
          List<Weather>.from(map['weather']?.map((x) => Weather.fromMap(x))),
      speed: map['speed']?.toDouble(),
      deg: map['deg']?.toInt(),
      gust: map['gust']?.toDouble(),
      clouds: map['clouds']?.toInt(),
      pop: map['pop']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory MyList2.fromJson(String source) =>
      MyList2.fromMap(json.decode(source));

  @override
  String toString() {
    return 'List(dt: $dt, sunrise: $sunrise, sunset: $sunset, temp: $temp, feels_like: $feels_like, pressure: $pressure, humidity: $humidity, weather: $weather, speed: $speed, deg: $deg, gust: $gust, clouds: $clouds, pop: $pop)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MyList2 &&
        other.dt == dt &&
        other.sunrise == sunrise &&
        other.sunset == sunset &&
        other.temp == temp &&
        other.feels_like == feels_like &&
        other.pressure == pressure &&
        other.humidity == humidity &&
        listEquals(other.weather, weather) &&
        other.speed == speed &&
        other.deg == deg &&
        other.gust == gust &&
        other.clouds == clouds &&
        other.pop == pop;
  }

  @override
  int get hashCode {
    return dt.hashCode ^
        sunrise.hashCode ^
        sunset.hashCode ^
        temp.hashCode ^
        feels_like.hashCode ^
        pressure.hashCode ^
        humidity.hashCode ^
        weather.hashCode ^
        speed.hashCode ^
        deg.hashCode ^
        gust.hashCode ^
        clouds.hashCode ^
        pop.hashCode;
  }
}
