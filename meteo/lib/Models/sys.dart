import 'dart:convert';

class Sys {
  final String pod;
  Sys({
    required this.pod,
  });

  Sys copyWith({
    String? pod,
  }) {
    return Sys(
      pod: pod ?? this.pod,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'pod': pod,
    };
  }

  factory Sys.fromMap(Map<String, dynamic> map) {
    return Sys(
      pod: map['pod'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Sys.fromJson(String source) => Sys.fromMap(json.decode(source));

  @override
  String toString() => 'Sys(pod: $pod)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Sys &&
      other.pod == pod;
  }

  @override
  int get hashCode => pod.hashCode;
}