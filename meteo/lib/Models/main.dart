import 'dart:convert';

class Main {
  final double temp;
  final double feels_like;
  final double temp_min;
  final double temp_max;
  final int pressure;
  final int sea_level;
  final int grnd_level;
  final int humidity;
  final double temp_kf;
  Main({
    required this.temp,
    required this.feels_like,
    required this.temp_min,
    required this.temp_max,
    required this.pressure,
    required this.sea_level,
    required this.grnd_level,
    required this.humidity,
    required this.temp_kf,
  });

  Main copyWith({
    double? temp,
    double? feels_like,
    double? temp_min,
    double? temp_max,
    int? pressure,
    int? sea_level,
    int? grnd_level,
    int? humidity,
    double? temp_kf,
  }) {
    return Main(
      temp: temp ?? this.temp,
      feels_like: feels_like ?? this.feels_like,
      temp_min: temp_min ?? this.temp_min,
      temp_max: temp_max ?? this.temp_max,
      pressure: pressure ?? this.pressure,
      sea_level: sea_level ?? this.sea_level,
      grnd_level: grnd_level ?? this.grnd_level,
      humidity: humidity ?? this.humidity,
      temp_kf: temp_kf ?? this.temp_kf,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'temp': temp,
      'feels_like': feels_like,
      'temp_min': temp_min,
      'temp_max': temp_max,
      'pressure': pressure,
      'sea_level': sea_level,
      'grnd_level': grnd_level,
      'humidity': humidity,
      'temp_kf': temp_kf,
    };
  }

  factory Main.fromMap(Map<String, dynamic> map) {
    return Main(
      temp: map['temp']?.toDouble(),
      feels_like: map['feels_like']?.toDouble(),
      temp_min: map['temp_min']?.toDouble(),
      temp_max: map['temp_max']?.toDouble(),
      pressure: map['pressure']?.toInt(),
      sea_level: map['sea_level']?.toInt(),
      grnd_level: map['grnd_level']?.toInt(),
      humidity: map['humidity']?.toInt(),
      temp_kf: map['temp_kf']?.toDouble(),
    );
  }

  String toJson() => json.encode(toMap());

  factory Main.fromJson(String source) => Main.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Main(temp: $temp, feels_like: $feels_like, temp_min: $temp_min, temp_max: $temp_max, pressure: $pressure, sea_level: $sea_level, grnd_level: $grnd_level, humidity: $humidity, temp_kf: $temp_kf)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Main &&
      other.temp == temp &&
      other.feels_like == feels_like &&
      other.temp_min == temp_min &&
      other.temp_max == temp_max &&
      other.pressure == pressure &&
      other.sea_level == sea_level &&
      other.grnd_level == grnd_level &&
      other.humidity == humidity &&
      other.temp_kf == temp_kf;
  }

  @override
  int get hashCode {
    return temp.hashCode ^
      feels_like.hashCode ^
      temp_min.hashCode ^
      temp_max.hashCode ^
      pressure.hashCode ^
      sea_level.hashCode ^
      grnd_level.hashCode ^
      humidity.hashCode ^
      temp_kf.hashCode;
  }
}