// ignore: file_names
// ignore_for_file: file_names

import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'city_1.dart';
import 'list_1.dart';

class MeteoDay {
  final String cod;
  final int message;
  final int cnt;
  final List<MyList2> list;
  final City city;
  MeteoDay({
    required this.cod,
    required this.message,
    required this.cnt,
    required this.list,
    required this.city,
  });

  MeteoDay copyWith({
    String? cod,
    int? message,
    int? cnt,
    List<MyList2>? list,
    City? city,
  }) {
    return MeteoDay(
      cod: cod ?? this.cod,
      message: message ?? this.message,
      cnt: cnt ?? this.cnt,
      list: list ?? this.list,
      city: city ?? this.city,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'cod': cod,
      'message': message,
      'cnt': cnt,
      'list': list,
      'city': city.toMap(),
    };
  }

  factory MeteoDay.fromMap(Map<String, dynamic> map) {
    List<MyList2> f = [];
    for (int i = 0; i < map['list']?.length; i++) {
      f.add(MyList2.fromMap(map['list']?[i]));
    }
    return MeteoDay(
      cod: map['cod'],
      message: map['message']?.toInt(),
      cnt: map['cnt']?.toInt(),
      list: f,
      city: City.fromMap(map['city']),
    );
  }

  String toJson() => json.encode(toMap());

  factory MeteoDay.fromJson(String source) =>
      MeteoDay.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Empty(cod: $cod, message: $message, cnt: $cnt, list: $list, city: $city)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MeteoDay &&
        other.cod == cod &&
        other.message == message &&
        other.cnt == cnt &&
        listEquals(other.list, list) &&
        other.city == city;
  }

  @override
  int get hashCode {
    return cod.hashCode ^
        message.hashCode ^
        cnt.hashCode ^
        list.hashCode ^
        city.hashCode;
  }
}
