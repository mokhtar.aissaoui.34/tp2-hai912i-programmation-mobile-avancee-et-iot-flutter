import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:meteo/Models/MeteoDay.dart';
import 'package:meteo/Models/empty.dart';
import 'package:meteo/Models/finalModel.dart';
import 'package:meteo/bloc/meteo.cubit.dart';

class MeteoPage extends StatelessWidget {
  MeteoPage({Key? key}) : super(key: key);
  String getUrl(String WeatherDesciption) {
    switch (WeatherDesciption) {
      case "Clear":
        {
          return "https://www.francestickers.com/4261-thickbox/stickers-soleil-.jpg";
        }
      case "Clouds":
        {
          return "https://www.francestickers.com/4265-thickbox/stickers-soleil-et-nuages.jpg";
        }
      case "Rain":
        {
          return "https://www.francestickers.com/4267-thickbox/stickers-nuage-et-pluie.jpg";
        }
      case "Snow":
        {
          return "https://previews.123rf.com/images/dvarg/dvarg1207/dvarg120700528/14657595-ic%C3%B4ne-m%C3%A9t%C3%A9o-unique-cloud-avec-la-neige-et-la-pluie.jpg";
        }
      default:
        {
          return "https://www.francestickers.com/4261-thickbox/stickers-soleil-.jpg";
        }
    }
  }

  Widget Semaine(int i, MeteoDay j) {
    String url = getUrl(j.list[i].weather[0].main);
    return Container(
      width: 200.0,
      padding: const EdgeInsets.all(1),
      margin: const EdgeInsets.all(4),
      decoration: BoxDecoration(
          color: const Color.fromRGBO(189, 240, 255, 1),
          borderRadius: BorderRadius.circular(25)),
      child: Stack(
        children: [
          Positioned(
              top: 35,
              left: 70,
              child: CircleAvatar(
                radius: 30.0,
                backgroundImage: NetworkImage(url),
              )),
          Positioned(
              left: 25,
              top: 10,
              child: Text(DateFormat('EEEE d MMMM yyyy')
                  .format(DateTime.now().add(Duration(days: i))))),
          Positioned(
              left: 40,
              top: 110,
              child: Column(
                children: [
                  Text("Temp min:" + j.list[i].temp.min.toString() + " °C"),
                  Text("Temp max:" + j.list[i].temp.max.toString() + " °C"),
                  Text("Hum :" + j.list[i].humidity.toString() + "%"),
                  Text("Vent :" + j.list[i].speed.toString() + "ml/h"),
                ],
              ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MeteoCubit, finalModel>(builder: (context, state) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          children: [
            Column(
              children: <Widget>[
                Container(
                    height: 150,
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(
                                "https://www.portailmeteo.com/wp-content/uploads/2020/11/meteo-15-jours.jpg"),
                            fit: BoxFit.cover))),
                TextField(
                  onSubmitted: (text) {
                    context.read<MeteoCubit>().MajCity(text);
                  },
                  decoration: const InputDecoration(
                      fillColor: Colors.black,
                      border: UnderlineInputBorder(),
                      prefixIcon: Icon(Icons.search),
                      labelText: "Enter city"),
                ),
                SizedBox(
                    height: 350,
                    child: Center(
                        child: Column(children: [
                      Text(state.now.city.name,
                          style: const TextStyle(
                            fontSize: 30,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          )),
                      Text(DateFormat('EEEE d MMMM yyyy – kk:mm')
                          .format(DateTime.now())),
                      Container(
                          height: 100,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                  getUrl(state.now.list[0].weather[0].main)),
                            ),
                          )),
                      Text(state.now.list[0].main.temp.toString() + " °C",
                          style: const TextStyle(
                            fontSize: 30,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          )),
                      const Spacer(),
                      IntrinsicHeight(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            SizedBox(
                              width: 40,
                              child: Column(children: [
                                Text(state.now.list[0].main.temp_kf.toString() +
                                    "ml/h"),
                                const Icon(Icons.air)
                              ]),
                            ),
                            const VerticalDivider(),
                            SizedBox(
                              width: 40,
                              child: Column(children: [
                                Text(
                                    state.now.list[0].main.humidity.toString() +
                                        "%"),
                                const Icon(Icons.umbrella_outlined)
                              ]),
                            ),
                            const VerticalDivider(),
                            SizedBox(
                              width: 80,
                              child: Column(children: [
                                Text(state.now.list[0].main.temp_min
                                        .toString() +
                                    "-" +
                                    state.now.list[0].main.temp_max.toString() +
                                    " °C"),
                                const Icon(Icons.thermostat)
                              ]),
                            )
                          ],
                        ),
                      ),
                      const Spacer(),
                    ]))),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 20.0),
                  height: 200.0,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Semaine(0, state.week),
                      Semaine(1, state.week),
                      Semaine(2, state.week),
                      Semaine(3, state.week),
                      Semaine(4, state.week),
                      Semaine(5, state.week),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    });
  }
}
