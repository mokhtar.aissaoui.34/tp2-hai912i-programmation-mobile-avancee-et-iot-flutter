import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:meteo/Models/MeteoDay.dart';
import 'package:meteo/Models/empty.dart';
import 'package:meteo/Models/finalModel.dart';

class Network {
  Future<finalModel> getWeatherForecast({required String cityName}) async {
    String appid = "d297f84f1932ef087ed0a9853939f187";
    var finalUrl = "https://api.openweathermap.org/data/2.5/forecast?q=" +
        cityName +
        "&units=metric" +
        "&appid=" +
        appid;
    var finalUrl2 =
        "https://pro.openweathermap.org/data/2.5/forecast/daily?q=" +
            cityName +
            "&units=metric" +
            "&appid=" +
            appid;

    final response = await http.get(Uri.parse(finalUrl));
    final response2 = await http.get(Uri.parse(finalUrl2));

    if (response.statusCode == 200 && response2.statusCode == 200) {
      //we get the actual mappped model (dart object)
      //print("weather data : ${response.body}");
      return finalModel(
          Empty.fromJson(response.body), MeteoDay.fromJson(response2.body));
    } else {
      throw Exception("Error getting weather forecast");
    }
  }
}
