import 'package:meteo/Models/MeteoDay.dart';
import 'package:meteo/Models/empty.dart';

import 'package:bloc/bloc.dart';
import 'package:meteo/Models/finalModel.dart';
import 'package:meteo/network/Network.meteo.dart';

class MeteoCubit extends Cubit<finalModel> {
  MeteoCubit(initialState) : super(initialState);
  void MajCity(String city) {
    Network n = Network();
    Future<finalModel> futureWeather = n.getWeatherForecast(cityName: city);
    try {
      futureWeather.then((value) => emit(value));
    } catch (e) {
      print(e.toString());
    }
  }
}
